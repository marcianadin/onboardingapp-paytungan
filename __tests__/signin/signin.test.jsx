import { render, screen } from '@testing-library/react'
import Signin from '../../pages/login'

jest.mock('firebase/app', () => {
  const initializeApp = jest.fn()
  const getApp = jest.fn()
  const getApps = jest.fn(() => {
    return {
      length: 1
    }
  })

  return {
    getApp: getApp,
    getApps: getApps,
    initializeApp: initializeApp
  }
})

jest.mock('firebase/auth')

describe('Singin', () => {
  it('test Signin UI page', () => {
    render(<Signin />)
    expect(screen.getByText('Sign In')).toBeInTheDocument()
    expect(screen.getByRole('btn-masuk')).toBeVisible()
    expect(screen.getByText('Belum memiliki akun?')).toBeInTheDocument()
    expect(screen.getByRole('btn-sign-up')).toBeVisible()
    expect(screen.getByRole('btn-sign-up')).toHaveTextContent('Sign Up')
    expect(screen.getByText('Semua Tagihan Dalam Satu Waktu')).toBeInTheDocument()
    expect(screen.getByText('Seluruhnya akan tergabung menjadi dalam satu waktu untuk penagihan!')).toBeInTheDocument()
  })
})
