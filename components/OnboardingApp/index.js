import React from "react"
import { css } from "twin.macro"

const OnboardingApp = props => {
  if (!props.show) {
    return null
  }
  return (
    <>
      <div
        tw="bg-hitam"
        css={css`
          position: fixed;
          opacity: 0.5;
          top: -5%;
          height: 110vh;
          width: 500px;
        `}
      ></div>
      <div tw="flex items-center justify-evenly rounded-t-2xl">
        <div
          tw="box-content p-5 rounded-lg w-60 h-28 border-white bg-white"
          css={css`
            position: fixed;
            top: 26%;
          `}
        >
          <div tw="grid justify-center mt-5">
          <div tw="">
              <p tw="font-bold text-center mb-2">
                {" "}
                Input wording onboarding app{" "}
              </p>
            </div>
            <div tw="flex justify-center ml-2">
              <button>
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default OnboardingApp
